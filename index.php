<?php
$file = "date.csv";
$openfile = file_get_contents($file);
$exists = preg_match("~[\d]{2}.[\d]{2}.[\d]{4}~", $openfile);
if(!$exists){
    // Массив с именами $arrNames
    $arrNames = ['Иван 1','Иван 2','Олег','Дмитрий'];
    // Создаем цикл для сегодняшнего и на 3 месяца вперед
    for ($i = 0; $i<4; $i++){
        // Устанавливаем дату на 1 месяц вперед
        $date = strtotime("+ {$i} month");
        // Задаем формат даты
        $dateN = date("d.m.Y", $date);
        // Достаем первый и третий понедельник месяца
        $next_month_first_monday_ts = strtotime("first monday", strtotime($dateN));
        $next_month_third_monday_ts = strtotime("third monday", strtotime($dateN));
        // Задаем формат даты
        $third_date = date("d.m.Y", $next_month_third_monday_ts);
        $first_date = date("d.m.Y", $next_month_first_monday_ts);
        $randInt1 = rand(100, 999);
        $randInt2 = rand(100, 999);
        // Генерируем 2 случайных имени с массива
        $rand_keys = array_rand($arrNames, 2);
        $randName1 = $arrNames[$rand_keys[0]];
        $randName2 = $arrNames[$rand_keys[1]];
        // Записываем в переменную $str
        $str = $first_date . "," . $randInt1 . "," . $randName1 . ";\n". $third_date . "," . $randInt2 . "," . $randName2 . ";\n";
        $openfile .= $str;
    }
    // Записываем в файл
    file_put_contents($file,$openfile);
}